package main

import (
	"context"
	"fmt"

	"gitlab.com/rwxrob/cmdtab"
	"gitlab.com/rwxrob/live"
)

func init() {
	x := cmdtab.New("service")
	x.Parameters = "start stop tail"
	x.Usage = `(start|stop|tail)`
	x.Summary = `controls daemon (service) running in background`

	x.Description = `
		The *service* subcommand is used to manage a background service
		(daemon) that has the primary responsibility to receive chat and
		events from Restream over websockets and log them to rotated files
		on the local host enabling more direct and efficient queries of them
		locally. This also ensures the best memory management since chat and
		events are never stored in the memory of the running service but
		flushed immediately to disk.`

	x.Method = func(args []string) error {
		if len(args) == 0 {
			return x.UsageError()
		}
		switch args[0] {
		case "start":
			ctx, cancel := context.WithCancel(context.Background())
			// TODO go live.LogChat(ctx)
			go live.LogEvents(ctx)
			// TODO go live.RotateLogs(ctx)
			cmdtab.WaitForInterrupt(cancel)
		case "stop":
			// TODO
			fmt.Println("would stop")
		case "tail":
			// TODO
			fmt.Println("would tail")
		default:
			return fmt.Errorf("unsupported server action: %v", args[0])
		}
		return nil
	}

}
