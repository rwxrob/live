package main

import "gitlab.com/rwxrob/cmdtab"

func init() {
	x := cmdtab.New("live", "wschat", "wsevents", "service", "dir")
	x.Summary = `command line helper for terminal-using live streamers`
	x.Version = "1.0.0"
	x.Author = "Rob Muhlestein <rwx@robs.io> (rwxrob.live)"
	x.Git = "gitlab.com/rwxrob/live/cmd/live"
	x.Copyright = "(c) Rob Muhlestein"
	x.License = "Apache-2.0"

	x.Description = `
		The *live* command line utility is designed for those who prefer the
		terminal and do a lot of live streaming interaction primarily with
		Restream, Twitch, YouTube, and Discord.

		Dependency on *auth*

		The *auth* command is just short of a requirement to be able to use
		*live* since *live* depends so heavily on keeping authorization
		information up to date and safely cached. See
		https://gitlab.com/rwxrob/auth for more about it.`

}
