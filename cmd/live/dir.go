package main

import (
	"fmt"

	"gitlab.com/rwxrob/cmdtab"
	"gitlab.com/rwxrob/live"
)

func init() {
	x := cmdtab.New("dir")
	x.Summary = `prints the current data directory used for logging`

	x.Description = `
		The *dir* subcommand simply prints the full path to the current data
		directory as determined by whether LIVE_DATA_DIR, or XDG_DATA_DIR
		are set as environment variables, or if the default
		(HOME/.local/share/live) directory is used.`

	x.Method = func(args []string) error {
		fmt.Println(live.LiveDataDir())
		return nil
	}

}
