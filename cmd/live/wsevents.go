package main

import (
	"fmt"
	"os"
	"os/signal"

	ws "github.com/gorilla/websocket"
	"gitlab.com/rwxrob/auth"
	"gitlab.com/rwxrob/cmdtab"
)

func init() {
	x := cmdtab.New("wsevents")
	x.Summary = `redirects incoming Restream websocket JSON *events* to stdout`

	x.Description = `
		The *wsevents* subcommand connects to the Restream.io service and it's
		websocket API for receiving all incoming event messages. In UNIX
		philosophy fashion, each message is written directly to standard
		output one line at a time with the JSON data for a single message
		contained fully within a single line. This enables creation of FIFO
		and UNIX pipelines to other commands of any kind including simply to
		the *jq* command.`

	x.Method = func(args []string) error {
		_, app, err := auth.Use("restream")
		if err != nil {
			return err
		}
		err = app.Refresh()
		if err != nil {
			return err
		}

		interrupt := make(chan os.Signal, 1)
		signal.Notify(interrupt, os.Interrupt)

		url := "wss://streaming.api.restream.io/ws?accessToken=" +
			app.AccessToken
		conn, _, err := ws.DefaultDialer.Dial(url, nil)
		if err != nil {
			return err
		}
		defer conn.Close()

		go func() {
			for {
				_, msg, err := conn.ReadMessage() // blocks waiting
				fmt.Println(string(msg))
				if err != nil {
					return
				}
			}
		}()

		for {
			// TODO upgrade this to a multi-channel select
			select {
			case <-interrupt:
				err := conn.WriteMessage(ws.CloseMessage,
					ws.FormatCloseMessage(ws.CloseNormalClosure, ""))
				if err != nil {
					return err
				}
				return nil
			}
		}

		return nil
	}

}
