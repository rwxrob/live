package live

import (
	"context"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/rwxrob/auth"
)

// LiveDataDir returns the content of the LIVE_DATA_DIR system
// environment variable, or XDG_DATA_HOME/live if set, or
// HOME/.local/share/live within the current user's home directory, or,
// in the extreme exception that the current user's home directory
// cannot be determined the current working directory will be used (.).
func LiveDataDir() string {
	live := os.Getenv("LIVE_DATA_DIR")
	if live != "" {
		return live
	}
	live = os.Getenv("XDG_DATA_HOME")
	if live != "" {
		return filepath.Join(live, "live")
	}
	live, err := os.UserHomeDir()
	if err != nil {
		return "."
	}
	return filepath.Join(live, ".local/share/live")
}

// LogChat connects to Restream websocket service and begins logging to
// the chat log within LiveDataDir().  Each line of the logs is a JSON
// record as defined by the Restream API. This functions blocks so best
// to call as a goroutine. Consider also starting RotateLogs() to manage
// the log files concurrently. Consider the Tail*() functions that
// return the last lines of these logs when consuming the log data.  Or
// simply use the equivalent commands available on your given operating
// system.
func LogChat(ctx context.Context) {

	dir := LiveDataDir()
	path := filepath.Join(dir, "chat")

	// create the dirctory if not available
	err := os.MkdirAll(dir, 0700)
	if err != nil {
		log.Println(err)
		return
	}

	// open the file for appending
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	defer file.Close()
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("logging events to file: %v\n", path)

	// get authorization
	// FIXME this is forcing a grant every time
	_, app, err := auth.Use("restream")
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("received restream authorization")

	// dial up the service
	url := "wss://streaming.api.restream.io/ws?accessToken=" +
		app.AccessToken
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	log.Println("connected to events websocket")

	// loop, read message and log it until shut down
	first := true
	for {
		select {
		case <-ctx.Done():
			log.Println("shutting down event log server")
			return
		default:
			_, msg, err := conn.ReadMessage() // blocks waitin
			if first {
				log.Println("received first event from restream")
				first = false
			}
			_, err = file.WriteString(string(msg))
			if err != nil {
				log.Println(err)
				return
			}
		}
	}
}

// LogEvents connects to Restream websocket service and begins logging to
// the event log within LiveDataDir().  Each line of the logs is a JSON
// record as defined by the Restream API. This functions blocks so best
// to call as a goroutine. Consider also starting RotateLogs() to manage
// the log files concurrently. Consider the Tail*() functions that
// return the last lines of these logs when consuming the log data.  Or
// simply use the equivalent commands available on your given operating
// system.
func LogEvents(ctx context.Context) {

	dir := LiveDataDir()
	path := filepath.Join(dir, "events")

	// create the dirctory if not available
	err := os.MkdirAll(dir, 0700)
	if err != nil {
		log.Println(err)
		return
	}

	// open the file for appending
	file, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	defer file.Close()
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("logging events to file: %v\n", path)

	// get authorization
	// FIXME this is forcing a grant every time
	_, app, err := auth.Use("restream")
	if err != nil {
		log.Println(err)
		return
	}
	log.Println("received restream authorization")

	// dial up the service
	url := "wss://streaming.api.restream.io/ws?accessToken=" +
		app.AccessToken
	conn, _, err := websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()
	log.Println("connected to events websocket")

	// loop, read message and log it until shut down
	first := true
	for {
		select {
		case <-ctx.Done():
			log.Println("shutting down event log server")
			return
		default:
			_, msg, err := conn.ReadMessage() // blocks waitin
			if first {
				log.Println("received first event from restream")
				first = false
			}
			_, err = file.WriteString(string(msg))
			if err != nil {
				log.Println(err)
				return
			}
		}
	}
}

// RotateLogs periodically checks the size of the log files in
// LiveDataDir() and rotates them by size (default: 200 KiB).
func RotateLogs(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			//fmt.Println("rotatelogs: done")
			return
		default:
			//fmt.Println("rotatelogs: checking")
			// TODO check chat and events logs and rotate
			time.Sleep(5 * time.Second)
		}
	}
}
