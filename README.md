# Go `live`, A Live Streamer Command Line Utility

![WIP](https://img.shields.io/badge/status-wip-red.svg)
[![GoDoc](https://godoc.org/gitlab.com/rwxrob/live?status.svg)](https://godoc.org/gitlab.com/rwxrob/live)
[![License](https://img.shields.io/badge/license-Apache-brightgreen.svg)](LICENSE)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/rwxrob/live)](https://goreportcard.com/report/gitlab.com/rwxrob/live)
[![Coverage](https://gocover.io/_badge/gitlab.com/rwxrob/live)](https://gocover.io/gitlab.com/rwxrob/live)


## Usage

```
live help
live wschat
live wsevents
```

## TODO

* Create *good* Discord notifications that contain the title of the
  stream (instead of the stupid Restream notifications that cannot be
  changed).
* Add point system for people helping out on the stream

## User Stories / Requirements

The whole goal of the system is to promote instructive, inclusive,
useful community participation in a fun way.

User Types (Actors)

* Live Stream Community Members (Chat User)
* Live Streamer 

As a chat user, I want ...

* to control my avatar, color, and name and synchronize across different
  chat services 

* to be able to see schedule of things happening

* to search the current and previous chat histories by date, keyword, etc.

* to send certain non-chat commands (`!`)

* use points to make things fun happen on the live stream

* rate the live streamer's current performance or agreement

* use emojis without the overhead of looking them up

* steer the direction of a session (perhaps through the type 
  of emojis submitted)

As a live streamer, I want ...

* to be give (and remove) points to people on the live stream when they
  make an interesting contribution

* to store the information about my community members so I can quickly
  look up information about them and display different things during a
  live stream (or off stream)

* put forth live, time-constrained voting on topics in a fun way

* count how many of a specific performance emoji has been entered
  recently and in real-time
