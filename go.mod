module gitlab.com/rwxrob/live

go 1.15

//replace gitlab.com/rwxrob/cmdtab => ../cmdtab

require (
	github.com/gorilla/websocket v1.4.2
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	gitlab.com/rwxrob/auth v1.2.2-alpha
	gitlab.com/rwxrob/cmdtab v0.2.0-alpha
	golang.org/x/tools v0.0.0-20201125231158-b5590deeca9b // indirect
)
