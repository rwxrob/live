package live

import (
	"os"
	"testing"
)

func TestLiveDataDir(t *testing.T) {
	t.Log(LiveDataDir())
	os.Setenv("XDG_DATA_HOME", "/tmp/share")
	t.Log(LiveDataDir())
	os.Setenv("LIVE_DATA_DIR", "/home/another/some")
	t.Log(LiveDataDir())
}
